# ChessX

ChessX is a free and open source chess database application for Linux, Mac OS X and Windows.

Home page: https://sourceforge.net/projects/chessx/

GitHub: https://github.com/Isarhamster/chessx/
